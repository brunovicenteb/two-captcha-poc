<br/>
<p align="center">
  <a href="https://github.com/brunovicenteb/two-captcha-poc">
    <img src="images/TwoCaptcha.svg" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">2Captcha library proof of concept</h3>

  <p align="center">
    A simple proof of concept written in .Net core 8 (C#) that allows you to crack a captcha using the 2Captcha API.

More information at: https://2captcha.com/
    <br/>
    <br/>
  </p>
</p>