﻿namespace TwoCaptcha_PoC;

public static class TwoCaptchaKey
{
    public const string TWO_CAPTCHA_API_KEY_VAR_NAME = "TWO_CAPTCHA_API_KEY";
    public static readonly string ApiKey = Environment.GetEnvironmentVariable(TWO_CAPTCHA_API_KEY_VAR_NAME);
}