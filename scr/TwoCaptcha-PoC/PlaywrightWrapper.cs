﻿using Microsoft.Playwright;

namespace TwoCaptcha_PoC;

public class PlaywrightWrapper : IDisposable
{
    private static readonly TimeSpan _defaultTimeout = TimeSpan.FromMinutes(1);
    private static readonly object _toLock = new();
    private static IBrowser _browser;
    private readonly IBrowserContext _context;

    private PlaywrightWrapper(IBrowserContext context)
    {
        _context = context;
    }

    public static PlaywrightWrapper CreateContext()
    {
        lock (_toLock)
        {
            if (_browser == null)
            {
                var browserInstance = Playwright.CreateAsync().Result;
                _browser = browserInstance.Chromium.LaunchAsync(new()
                {
                    Headless = false,
                    Channel = "chrome",
                    Timeout = (int)_defaultTimeout.TotalMilliseconds
                }).Result;
            }
            var browserCtx = _browser.NewContextAsync().Result;
            browserCtx.SetDefaultTimeout((int)_defaultTimeout.TotalMilliseconds);
            return new PlaywrightWrapper(browserCtx);
        }
    }

    public async Task SetTextByKeyboardAsync(string selector, string value)
    {
        await ClickOnSelectorAsync(selector);
        var page = await GetPageAsync();
        for (int i = 0; i < value.Length; i++)
        {
            await page.Keyboard.TypeAsync(new string(value[i], 1));
            await Task.Delay(Random.Shared.Next(50, 75));
        }
        await page.Keyboard.PressAsync("Tab");
    }

    public async Task<ILocator> ClickOnSelectorAsync(string locator)
    {
        var page = await GetPageAsync();
        var locatorCode = page.Locator(locator)
            .AllAsync()
            .Result
            .First();
        await locatorCode.ClickAsync();
        return locatorCode;
    }

    public async Task<IPage> GetPageAsync()
    {
        if (_context.Pages.Count > 0)
            return _context.Pages.First();
        return await _context.NewPageAsync();
    }

    public void Dispose()
    {
        _context?.CloseAsync().Wait();
        _context?.DisposeAsync();
    }
}