﻿using TwoCaptcha;
using TwoCaptcha_PoC;

const string LINK = "https://wwws.bradescoseguros.com.br/TELE-WEB3270/entry";

try
{
    using var browser = PlaywrightWrapper.CreateContext();
    var page = await browser.GetPageAsync();
    await page.GotoAsync(LINK);

    await browser.SetTextByKeyboardAsync("#form1\\:textUser1", "AnyPassword");
    await browser.ClickOnSelectorAsync("#form1\\:button2");
    await page.WaitForLoadStateAsync();

    var imageLocator = page.Locator("body > img:nth-child(2)").First;
    var attribute = await imageLocator.GetAttributeAsync("src");

    var solver = new Solve();
    var result = await solver.NormalCaptchaAsync(TwoCaptchaKey.ApiKey, attribute.ToString());
    if (result.Request == "ERROR_ZERO_BALANCE")
        throw new Exception("Tu não tem dinheiro rapaz!");

    await browser.SetTextByKeyboardAsync("#ans", result.Request);
    await browser.ClickOnSelectorAsync("#jar");

    Console.WriteLine($"Captcha quebrado corretamente. O texto é: {result.Request}");
    Console.ReadLine();
}
catch (Exception ex)
{
    Console.WriteLine($"Deu ruim: {ex.Message}");
    Console.ReadLine();
}